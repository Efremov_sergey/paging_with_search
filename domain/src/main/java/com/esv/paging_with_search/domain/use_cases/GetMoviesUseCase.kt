package com.esv.paging_with_search.domain.use_cases

import com.esv.paging_with_search.domain.repository.MovieRepository

class GetMoviesUseCase(private val movieRepository: MovieRepository) {
    fun execute() = movieRepository.loadMovies()
}