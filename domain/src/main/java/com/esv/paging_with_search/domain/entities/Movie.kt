package com.esv.paging_with_search.domain.entities

import java.util.*

data class Movie(
    val id: Int,
    val title: String,
    val date: Date
)
