package com.esv.paging_with_search.domain.repository

import com.esv.paging_with_search.domain.entities.Movie
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    fun loadMovies(): Flow<List<Movie>>
}