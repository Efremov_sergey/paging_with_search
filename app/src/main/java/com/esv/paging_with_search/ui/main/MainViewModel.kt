package com.esv.paging_with_search.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.esv.paging_with_search.data.repository.MovieRepositoryImpl
import com.esv.paging_with_search.domain.repository.MovieRepository

class MainViewModel(
    movieRepository: MovieRepositoryImpl
) : ViewModel() {
    val movieList = movieRepository.loadMovies().asLiveData()
}