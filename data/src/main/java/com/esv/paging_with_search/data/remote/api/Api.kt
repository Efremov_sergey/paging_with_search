package com.esv.paging_with_search.data.remote.api

import com.esv.paging_with_search.data.remote.model.Movie
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface Api {
    @GET(POPULAR)
    suspend fun getDaily(): List<Movie>

    companion object {
        private const val BASE_URL = "http://api.themoviedb.org/3/"
        private const val POPULAR = "/movie/popular/"

        var retrofitService: Api? = null

        fun getInstance() : Api {
            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(Api::class.java)
            }
            return retrofitService!!
        }
    }
}
