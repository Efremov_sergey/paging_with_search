package com.esv.paging_with_search.di

import com.esv.paging_with_search.data.repository.MovieRepositoryImpl
import com.esv.paging_with_search.data.storage.AppDatabase
import com.esv.paging_with_search.ui.main.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { MainViewModel(MovieRepositoryImpl(get())) }
}

val repoModule = module {
    single { MovieRepositoryImpl(get()) }
}

val databaseModule = module {
    single { AppDatabase.getInstance(androidContext()) }
    single { AppDatabase.getInstance(androidContext()).movieDao }
}