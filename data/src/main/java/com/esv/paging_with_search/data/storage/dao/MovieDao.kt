package com.esv.paging_with_search.data.storage.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.esv.paging_with_search.data.storage.model.Movie
import kotlinx.coroutines.flow.Flow

@Dao
interface MovieDao {
    @Query("SELECT * FROM movie")
    fun getAll(): PagingSource<Int, Movie>

    @Insert
    fun insertAll(list: List<Movie>)
}