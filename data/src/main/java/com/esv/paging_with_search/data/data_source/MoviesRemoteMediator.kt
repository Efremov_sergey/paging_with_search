package com.esv.paging_with_search.data.data_source

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.esv.paging_with_search.data.remote.api.Api
import com.esv.paging_with_search.data.storage.dao.MovieDao
import com.esv.paging_with_search.data.storage.model.Movie
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

@OptIn(ExperimentalPagingApi::class)
class MoviesRemoteMediator(
    private val movieDao: MovieDao,
    private val service: Api
) : RemoteMediator<Int, Movie>() {
    override suspend fun load(loadType: LoadType, state: PagingState<Int, Movie>): MediatorResult {
        return try {
            val loadKey = when (loadType) {
                LoadType.REFRESH -> null
                LoadType.PREPEND ->
                    return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    val lastItem = state.lastItemOrNull()
                    if (lastItem == null) {
                        return MediatorResult.Success(
                            endOfPaginationReached = true
                        )
                    }

                    lastItem.id
                }
            }

            val response = service.getDaily()

            CoroutineScope(Dispatchers.IO).launch {
//                if (loadType == LoadType.REFRESH) {
//                    userDao.deleteByQuery(query)
//                }
//                movieDao.insertAll(response)
            }

            MediatorResult.Success(
                endOfPaginationReached = false//response.nextKey == null
            )
        } catch (e: IOException) {
            MediatorResult.Error(e)
        } catch (e: HttpException) {
            MediatorResult.Error(e)
        }
    }
}