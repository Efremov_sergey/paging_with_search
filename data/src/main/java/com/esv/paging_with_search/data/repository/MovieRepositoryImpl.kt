package com.esv.paging_with_search.data.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import com.esv.paging_with_search.data.data_source.MoviesRemoteMediator
import com.esv.paging_with_search.data.storage.dao.MovieDao
import com.esv.paging_with_search.domain.entities.Movie
import com.esv.paging_with_search.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class MovieRepositoryImpl(
    private val movieDao: MovieDao
): MovieRepository {
    @OptIn(ExperimentalPagingApi::class)
    override fun loadMovies(): Flow<List<Movie>> = flow {
        Pager(
            config = PagingConfig(pageSize = 50),
            remoteMediator = MoviesRemoteMediator(movieDao)
        ) {
            movieDao.getAll()
        }
    }
}