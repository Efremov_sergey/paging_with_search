package com.esv.paging_with_search.data.remote.model

import com.google.gson.annotations.SerializedName

data class Movie(
    val id: Int,
    val title: String,
    @SerializedName("release_date")
    val releaseDate: String
)
